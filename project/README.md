# bigbang

> A Website to stream and book watching Movies

## Build Setup

``` bash
# make sure npm and nodejs are installed before continuing

# install dependencies
npm install

# create the database in couchdb by going to bigbang2/backend
node generate-db.js

#run server in bigbang2/backend
node server.js

# serve with host
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
